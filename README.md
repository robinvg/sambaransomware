# ransomweg

Reads samba audit log and blocks connections when a known ransomware extension is found 

## Install

```bash 
sudo cp bin/ransomweg /usr/local/bin/
sudo cp scripts/ransomweg.service /etc/systemd/system/
sudo mkdir /etc/ransomweg 
sudo cp scripts/ransomweg.cfg /etc/ransomweg/

sudo systemctl daemon-reload
sudo systemctl enable ransomweg.service 
sudo systemctl start ransomweg.service 
```

## Configure Samba 

Add this to /etc/samba/smb.conf
```bash 
full_audit: failure = none
full_audit: success = pwrite write rename
full_audit: prefix = IP=%I|USER=%u|MACHINE=%m|VOLUME=%S
full_audit: facility = local5
full_audit: priority = NOTICE
```

create `/etc/rsyslog.d/00-samba-audit.conf`
```bash
local5.notice /var/log/samba/audit.log 
& ~
```

Create log file 
```bash 
touch /var/log/samba/audit.log
```

Create logrotate file 

```bash 
vi /etc/logrotate.d/samba-audit 

/var/log/samba/audit.log{
    daily
    missingok
    rotate 7
    postrotate
        /bin/systemctl reload rsyslog.service > /dev/null 2>&1 || true
    endscript
    compress
    notifempty
}

```

## Iptables


### list blocked address 
```bash
ransomweg firewall --list
```
### unblock address
```bash
ransomweg firewall --unblock 1.2.3.4
```
### clear table 
```bash
ransomweg firewall --clear
```
### block address 
```bash
ransomweg firewall --block 1.2.3.4
```
