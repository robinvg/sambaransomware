package main

import (
	"artietee/smbrandsomware/iptables"
	"fmt"
	"os"
	"runtime"
	"strings"

	"github.com/spf13/cobra"
)

var defaultChainName = "SMBRANSOME"

func commandIPlist() *cobra.Command {
	IPlist := &cobra.Command{
		Use:   "firewall [...args]",
		Short: "iptables tool",
		Run: func(cmd *cobra.Command, args []string) {
			if err := ipTables(cmd, args); err != nil {
				fmt.Fprintf(os.Stderr, "Error: %v\n", err)
				os.Exit(1)
			}
		},
	}
	IPlist.Flags().String("log-level", "info", "Log level (one of panic, fatal, error, warn, info or debug)")
	IPlist.Flags().StringVar(&defaultChainName, "chain", defaultChainName, "Chain name")
	IPlist.Flags().Bool("list", false, "List blocked ip address")
	IPlist.Flags().String("delete", "", "Delete ip address")
	IPlist.Flags().String("block", "", "Block ip address")
	IPlist.Flags().Bool("clear", false, "Clear chain")
	IPlist.Flags().Bool("delete-chain", false, "Clear en delete chain")

	return IPlist
}

func ipTables(cmd *cobra.Command, args []string) error {
	var err error

	if runtime.GOOS != "linux" {
		fmt.Println("Can only be run on Linux")
		os.Exit(1)
	}

	chain, _ := cmd.Flags().GetString("chain")
	delete, _ := cmd.Flags().GetString("delete")
	block, _ := cmd.Flags().GetString("block")
	list, _ := cmd.Flags().GetBool("list")
	clear, _ := cmd.Flags().GetBool("clear")
	deleteChain, _ := cmd.Flags().GetBool("delete-chain")
	logLevel, _ := cmd.Flags().GetString("log-level")
	logger := newLogger("unix", strings.ToLower(logLevel))

	i, err := iptables.New(chain, logger)
	if err != nil {
		return err
	}
	if list {
		i.ListTables()
		os.Exit(0)
	}
	if clear {
		i.ClearChain()
		os.Exit(0)
	}
	if delete != "" {
		i.DeleteLine(delete)
		os.Exit(0)
	}
	if block != "" {
		i.BlockIP(block, false)
		os.Exit(0)
	}
	if deleteChain {
		i.DeleteChain()
		os.Exit(0)
	}
	return err
}
