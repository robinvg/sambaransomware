package main

import (
	"os"

	"github.com/rs/zerolog"
)

func newLogger(timeformat string, level string) *zerolog.Logger {
	if timeformat == "unix" {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}
	switch level {
	case "panic":
		zerolog.SetGlobalLevel(zerolog.PanicLevel)
	case "fatal":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "warn", "warning":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	case "disabled":
		zerolog.SetGlobalLevel(zerolog.Disabled)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
	if level == "trace" {
		logger = logger.With().Caller().Logger()
	}

	return &logger
}
