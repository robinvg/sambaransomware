package main

import (
	"artietee/smbrandsomware/server"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

var defaultLogLevel = "info"
var defaultSambaLog = "/var/log/syslog"
var defaultemail = false
var defaultSMTPHost = "localhost"
var defaultSMTPORT = 587
var defaultTesting = false
var defaultSMTPUsername = ""
var defaultSMTPPassword = ""
var defaultEmailFrom = ""
var defaultEmailTo = ""

func init() {
	if v := os.Getenv("firewall_chain"); v != "" {
		defaultChainName = v
	}
	if v := os.Getenv("log_level"); v != "" {
		defaultLogLevel = v
	}
	if v := os.Getenv("samba_log"); v != "" {
		defaultSambaLog = v
	}
	if v := os.Getenv("smtp_host"); v != "" {
		defaultemail = true
		defaultSMTPHost = v
	}
	if v := os.Getenv("smtp_port"); v != "" {
		defaultemail = true
		defaultSMTPORT, _ = strconv.Atoi(v)
	}
	if v := os.Getenv("smtp_username"); v != "" {
		defaultemail = true
		defaultSMTPUsername = v
	}
	if v := os.Getenv("smtp_password"); v != "" {
		defaultemail = true
		defaultSMTPPassword = v
	}
	if v := os.Getenv("email_from"); v != "" {
		defaultemail = true
		defaultEmailFrom = v
	}
	if v := os.Getenv("email_to"); v != "" {
		defaultemail = true
		defaultEmailTo = v
	}

}

func commandServe() *cobra.Command {
	serveCmd := &cobra.Command{
		Use:   "serve [...args]",
		Short: "Start service",
		Run: func(cmd *cobra.Command, args []string) {
			if err := serve(cmd, args); err != nil {
				fmt.Fprintf(os.Stderr, "Error: %v\n", err)
				os.Exit(1)
			}
		},
	}

	serveCmd.Flags().StringVar(&defaultLogLevel, "log-level", defaultLogLevel, "Log level (one of panic, fatal, error, warn, info or debug)")
	serveCmd.Flags().StringVar(&defaultSambaLog, "samba-log", defaultSambaLog, "Samba audit log")

	serveCmd.Flags().BoolVar(&defaultemail, "email", defaultemail, "Samba audit log")
	serveCmd.Flags().StringVar(&defaultSMTPHost, "smtp-host", defaultSMTPHost, "SMTP host")
	serveCmd.Flags().IntVar(&defaultSMTPORT, "smtp-port", defaultSMTPORT, "SMTP port (e.g. 25 or 587)")
	serveCmd.Flags().StringVar(&defaultSMTPUsername, "smtp-username", defaultSMTPUsername, "SMTP username")
	serveCmd.Flags().StringVar(&defaultSMTPPassword, "smtp-password", defaultSMTPPassword, "SMTP password file location")
	serveCmd.Flags().StringVar(&defaultEmailFrom, "email-from", defaultEmailFrom, "From address")
	serveCmd.Flags().StringVar(&defaultEmailTo, "email-to", defaultEmailTo, "To address")

	serveCmd.Flags().StringVar(&defaultChainName, "chain", defaultChainName, "chain name")
	serveCmd.Flags().BoolVar(&defaultTesting, "dry-run", defaultTesting, "Does not add any ip to iptables ")

	return serveCmd
}

func serve(cmd *cobra.Command, args []string) error {
	ctx := context.Background()
	var smtpPassword string
	logLevel, _ := cmd.Flags().GetString("log-level")

	logger := newLogger("unix", strings.ToLower(logLevel))
	sambaLog, _ := cmd.Flags().GetString("samba-log")
	email, _ := cmd.Flags().GetBool("email")

	smtpHost, _ := cmd.Flags().GetString("smtp-host")
	smtpPort, _ := cmd.Flags().GetInt("smtp-port")
	smtpUsername, _ := cmd.Flags().GetString("smtp-username")
	smtpPasswordFile, _ := cmd.Flags().GetString("smtp-password")
	chain, _ := cmd.Flags().GetString("chain")
	dryRun, _ := cmd.Flags().GetBool("dry-run")
	if smtpPasswordFile != "" {
		tmp, err := ioutil.ReadFile(smtpPasswordFile)
		if err != nil {
			logger.Fatal().Msg("unable to read password file")
		}
		smtpPassword = string(tmp)
	}
	emailTo, _ := cmd.Flags().GetString("email-to")
	emailFrom, _ := cmd.Flags().GetString("email-from")
	if email && (emailTo == "" || emailFrom == "") {
		logger.Fatal().Msg("--email-to and --email-from are mandorory if --email is used")
	}
	cfg := &server.Config{
		Logger:       logger,
		SambaLog:     sambaLog,
		Email:        email,
		EmailFrom:    emailFrom,
		EmailTo:      emailTo,
		SMTPHost:     smtpHost,
		SMTPPort:     smtpPort,
		SMTPUsername: smtpUsername,
		SMTPPassword: smtpPassword,
		Chain:        chain,
		DryRun:       dryRun,
	}

	srv, err := server.NewServer(cfg)
	if err != nil {
		return err
	}

	return srv.Serve(ctx)
}
