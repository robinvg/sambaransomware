/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 * Copyright 2018 Kopano and its licensors
 */

package main

import (
	"artietee/smbrandsomware/cmd"
	"fmt"
	"os"
)

func main() {

	cmd.RootCmd.AddCommand(commandServe())
	cmd.RootCmd.AddCommand(commandIPlist())
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
