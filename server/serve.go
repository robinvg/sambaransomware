package server

import (
	"artietee/smbrandsomware/iptables"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/hpcloud/tail"
	gomail "gopkg.in/mail.v2"
)

// type fn func([]byte) []byte
type StrSlice []string

//NewServer new log checker
func NewServer(c *Config) (*Server, error) {
	c.Logger.Info().Msg("start server")
	s := &Server{
		config: c,
		logger: c.Logger,
	}

	return s, nil
}

//Serve serve
func (s *Server) Serve(ctx context.Context) error {
	var err error
	var netClient = &http.Client{
		Timeout: time.Second * 10,
	}
	response, err := netClient.Get("https://fsrm.experiant.ca/api/v1/combined")
	if err != nil {
		s.logger.Error().Err(err).Msg("Unable to make request fsrm.experiant.ca")
	}
	defer response.Body.Close()

	//Create a variable of the same type as our model
	var r randsomWare
	//Decode the data
	if err := json.NewDecoder(response.Body).Decode(&r); err != nil {
		log.Fatal("ooopsss! an error occurred, please try again")
	}

	var ipt *iptables.Tables
	ipt, err = iptables.New(s.config.Chain, s.logger)
	if err != nil {
		return err
	}
	s.logger.Info().Msg("start tailing file")
	t, err := tail.TailFile(s.config.SambaLog, tail.Config{Follow: true, ReOpen: true})
	for line := range t.Lines {
		// fmt.Println(line.Text)
		splitLine := strings.Split(line.Text, "|")
		if len(splitLine) == 7 || len(splitLine) == 8 {
			ip := strings.Split(splitLine[0], "=")
			user := strings.Split(splitLine[1], "=")
			machine := strings.Split(splitLine[2], "=")
			volume := strings.Split(splitLine[3], "=")
			file := splitLine[6]
			if len(splitLine) == 8 {
				file = splitLine[7]
			}
			sb := sambaAudit{
				IP:      ip[len(ip)-1],
				user:    user[len(user)-1],
				machine: machine[len(machine)-1],
				Volume:  volume[len(volume)-1],
				action:  splitLine[4],
				status:  splitLine[5],
				oldfile: splitLine[6],
				file:    file,
			}
			extension := filepath.Ext(sb.file)
			if extension != "" {
				if contains(r.Filters, extension) {
					s.logger.Info().Msg("Found potential ransomware: " + sb.file + " IP " + sb.IP)
					if s.config.Email {
						sentMail(*s.config, sb)
					}

					if !s.config.DryRun {
						ipt.BlockIP(sb.IP, true)
					}
				}
			}
		}

	}
	return err

}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == "*"+str {
			return true
		}
	}

	return false
}

func sentMail(c Config, sm sambaAudit) {
	m := gomail.NewMessage()
	m.SetHeader("From", c.EmailFrom)
	m.SetHeader("To", c.EmailTo)
	m.SetHeader("Subject", "Ransomware attack ongoing")
	m.SetBody("text/plain", "file: "+sm.file+
		"\nip: "+sm.IP+
		"\nuser: "+sm.user+
		"\nMachine: "+sm.machine+
		"\nVolume: "+sm.Volume)
	// Settings for SMTP server
	d := gomail.NewDialer(c.SMTPHost, c.SMTPPort, c.SMTPUsername, c.SMTPPassword)

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
		panic(err)
	}
}
