package server

import "github.com/rs/zerolog"

// Config bundles configuration settings.
type Config struct {
	Logger       *zerolog.Logger
	SambaLog     string
	Email        bool
	SMTPHost     string
	SMTPPort     int
	SMTPUsername string
	SMTPPassword string
	EmailFrom    string
	EmailTo      string
	Chain        string
	DryRun       bool
}
