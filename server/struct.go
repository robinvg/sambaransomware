package server

import "github.com/rs/zerolog"

// Server ise out Nats implementation
type Server struct {
	config *Config
	logger *zerolog.Logger
}

type randsomWare struct {
	API struct {
		Version int
		Format  string
		Files   int `json:"file_group_count"`
	}
	Updated string `json:"lastUpdated"`
	Filters []string
}

type sambaAudit struct {
	IP      string
	user    string
	machine string
	Volume  string
	action  string
	status  string
	oldfile string
	file    string
}
