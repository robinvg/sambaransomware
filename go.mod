module artietee/smbrandsomware

go 1.17

require (
	github.com/coreos/go-iptables v0.6.0
	github.com/hpcloud/tail v1.0.0
	github.com/rs/zerolog v1.25.0
	github.com/spf13/cobra v1.2.1
	gopkg.in/mail.v2 v2.3.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
