package iptables

import (
	"github.com/coreos/go-iptables/iptables"
	"github.com/rs/zerolog"
)

// Config bundles configuration settings.
type Config struct {
	Chain  string
	Table  *iptables.IPTables
	Logger *zerolog.Logger
}
