package iptables

import (
	"fmt"

	"github.com/coreos/go-iptables/iptables"
	"github.com/rs/zerolog"
)

// Server ise out Nats implementation
type Tables struct {
	chain  string
	table  *iptables.IPTables
	logger *zerolog.Logger
}
type VSlice [][]string

func (s VSlice) String() string {
	var str string
	for _, i := range s {
		str += fmt.Sprintf("%s\n", i)
	}
	return str
}

//NewServer new log checker
func New(chain string, logger *zerolog.Logger) (*Tables, error) {
	ipt, err := iptables.New()
	if err != nil {
		logger.Fatal().Err(err).Msg("unable to get iptables list")
	}
	exists, err := ipt.ChainExists("filter", chain)
	if err != nil {
		logger.Fatal().Err(err).Msg("List failed")
	}
	if !exists {
		err := ipt.NewChain("filter", chain)
		if err != nil {
			logger.Fatal().Err(err).Msg("unable to create chain")
		}
		ipt.Insert("filter", "INPUT", 1, "-j", chain)
	}
	s := &Tables{
		chain:  chain,
		table:  ipt,
		logger: logger,
	}

	return s, nil
}
func (t *Tables) ListTables() {
	rules, err := t.table.Stats("filter", t.chain)
	if err != nil {
		t.logger.Fatal().Err(err).Msg("List failed")
	}
	fmt.Println(VSlice(rules))
}

func (t *Tables) ClearChain() {
	err := t.table.ClearChain("filter", t.chain)
	if err != nil {
		t.logger.Info().Msg("Chain '" + t.chain + "' successfully cleared ")
	} else {
		t.logger.Fatal().Msg("unable to clear chain: " + err.Error())
	}
}

func (t *Tables) DeleteChain() {
	err := t.table.ClearAndDeleteChain("filter", t.chain)
	if err != nil {
		t.logger.Info().Msg("Chain '" + t.chain + "' successfully deleted")
	} else {
		t.logger.Fatal().Msg("unable to delete chain: " + err.Error())
	}
}
func (t *Tables) DeleteLine(ip string) {
	err := t.table.DeleteIfExists("filter", t.chain, "-s", ip, "-p", "tcp", "--match", "multiport", "--dports", "137,138,139,445", "-j", "DROP")
	if err != nil {
		t.logger.Fatal().Err(err).Msg("unable to delete firewall rule")
	}
	fmt.Println(ip + " successfully removed")
}

func (t *Tables) BlockIP(ip string, logger bool) {
	var err error
	rules, err := t.table.Stats("filter", t.chain)
	if err != nil {
		t.logger.Fatal().Err(err).Msg("List failed")
	}
	// check if we already added this ip address
	for _, v := range rules {
		if v[7] == ip+"/32" {
			if logger {
				t.logger.Info().Msg("Already blocked")
			} else {
				fmt.Println("Already blocked")
			}
			return
		}
	}

	err = t.table.Insert("filter", t.chain, len(rules)+1, "-s", ip, "-p", "tcp", "--match", "multiport", "--dports", "137,138,139,445", "-j", "DROP")
	if err != nil {

		t.logger.Fatal().Err(err).Msg("Insert failed")
	}
	fmt.Println(ip + " successfully added")
}
