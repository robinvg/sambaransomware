build:
	GOOS=linux GOARCH=arm go build -o bin/ransomweg-linux-armhf cmd/sambaRansomware/*.go
	GOOS=linux GOARCH=arm64 go build -o bin/ransomweg-linux-arm64 cmd/sambaRansomware/*.go
	GOOS=linux GOARCH=amd64 go build -o bin/ransomweg-linux-amd64 cmd/sambaRansomware/*.go
	
run:
	go run *.go

dep: ## Get the dependencies
	@go get -v -d ./...

all:  compile